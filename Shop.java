import java.util.Scanner;
public class Shop {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);

        Phone[] phones = new Phone[4];

        for(int i = 0; i < 4; i++){
            System.out.println("Enter the brand of the phone: ");
            String brand = reader.next();
            System.out.println("Enter the model of the phone: ");
            String model = reader.next();
            System.out.println("Enter the price: ");
            double price = reader.nextDouble();
            phones[i] = new Phone(brand, model, price);
        }

        System.out.println("Enter the brand of the phone: ");
        String brand = reader.next();
        System.out.println("Enter the model of the phone: ");
        String model = reader.next();
        System.out.println("Enter the price: ");
        double price = reader.nextDouble();

        System.out.println("Before: " + phones[3].getBrand());
        System.out.println("Before: " + phones[3].getModel());
        System.out.println("Before: " + phones[3].getPrice());
        phones[3].setBrand(brand);
        phones[3].setModel(model);
        phones[3].setPrice(price);
        System.out.println("After: " + phones[3].getBrand());
        System.out.println("After: " + phones[3].getModel());
        System.out.println("After: " + phones[3].getPrice());

        System.out.println(phones[3].getBrand());
        System.out.println(phones[3].getModel());
        System.out.println(phones[3].getPrice());

        //printing what is returned from the instance method for the last product of the array
        System.out.println(phones[3].whatIsThePrice());
    }
}
