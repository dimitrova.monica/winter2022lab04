public class Phone {

    private String brand;
    private String model;
    private double price;
    //constructor
    public Phone(String brand, String model, double price){
        this.brand = brand;
        this.model = model;
        this.price = price;
    }
    public String whatIsThePrice(){
        if (price > 1200){
            return "Wowww, that's an expensive phone!";
        } else {
            return "This phone is not thattt expensive...I think.";
        }
    }
    //get methods
    public String getBrand(){
        return this.brand;
    }
    public String getModel(){
        return this.model;
    }
    public double getPrice(){
        return this.price;
    }

    //set methods
    //these can be deleted
    public void setModel(String newModel) {
        this.model = newModel;
    }
    public void setPrice(double newPrice) {
        this.price = newPrice;
    }
    public void setBrand(String newBrand) {
        this.brand = newBrand;
    }
}
